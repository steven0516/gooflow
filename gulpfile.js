var gulp = require('gulp');
var header = require('gulp-header');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var del = require('del');
var minifyCSS = require('gulp-minify-css');
var zip = require('gulp-zip');
var replace = require('gulp-replace-pro');
var pkg = require('./package.json');

var now = new Date(),month=now.getMonth()+1,date=now.getDate();
var strToday = now.getFullYear()+'-'+(month>9? '':'0')+month+'-'+(date>9? '':'0')+date;
console.log(pkg.version);

//给压缩文件增加头部注释的方法
function getHeader () {
	var template = ['/**',
		' * <%= pkg.name %> - <%= pkg.description %>',
		' * @author <%= pkg.author %>',
		' * @version v<%= pkg.version %>',
		' * @license <%= pkg.license %>',
		' **/',''
	].join('\n');
	return header(template, {
		pkg: pkg
	});
}
//获取版本号及最新发布日期的信息JSON
function getInfoJson () {
	var template = '{"version":"<%= pkg.version %>","publicDate":"<%= pkg.publicDate %>"}';
	return header(template, {
		pkg: {version:pkg.version,publicDate:strToday}
	});
}

//子任务：编写最新发布信息并覆盖保存在info.json中 only for ./examples
gulp.task('getInfo', function () {
	return gulp.src('./examples/info.json')
	.pipe(replace('^\{.*?\}$', ''))
	.pipe(getInfoJson()).pipe(gulp.dest('./examples',{overwrite:true}));
});
//子任务：清空原来的发布目录
gulp.task('clean', function () {
	return del(['./dist/**/*'], function(){});
});

//子任务：发布js文件
gulp.task('myJs', ['clean'], function(){
	return gulp.src(['src/GooFlow.*js'])
    .pipe(sourcemaps.init())
	.pipe(uglify())
	.pipe(getHeader())
	.pipe(rename({ extname: '.min.js' }))
    .pipe(sourcemaps.write('./maps'))
	.pipe(gulp.dest('./dist'));
});
//子任务：发布css文件
gulp.task('myCss', ['clean'], function(){
    return gulp.src('src/*.css')
	.pipe(minifyCSS({keepSpecialComments:0}))
	.pipe(getHeader())
	.pipe(rename({ extname: '.min.css' }))
	.pipe(gulp.dest('./dist'));
});
//子任务：发布其它资源文件
gulp.task('myAssets', ['clean'], function(){
    return gulp.src(['src/fonts/iconflow.*'],{base:'src'})
	.pipe(gulp.dest('./dist'));
});

//默认：子任务全部完成后打包并压缩成zip
gulp.task('default', ['getInfo','myJs','myCss','myAssets'], function() {
    return gulp.src("./dist/**/*",{base:'dist'})
     .pipe(zip('GooFlow-'+pkg.version+'.zip'))
	.pipe(gulp.dest('./dist'));
});
