requirejs.config({
    // 默认从js/lib加载所有的module ID
    baseUrl: './',
	map: {
		'*': {
			'css': './plugin/css.min.js' // https://github.com/guybedford/require-css, RequireJs's plugin
		}
	},
    paths: {
		jquery: './plugin/jquery.min',
        GooFlow: '../dist/GooFlow.min',
        'GooFlow.export': '../dist/GooFlow.export.min',  //可选，将流程图导出为图片文件的扩展包
        'GooFlow.print': '../dist/GooFlow.print.min',    //可选，将流程图输出打印或另存为PDF的扩展包
    },
    //// ……
    shim:{
        'GooFlow':{
            deps:['css!../dist/GooFlow.css','jquery']
        }
    },
});